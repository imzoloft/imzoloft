## <h3>Welcome</h3>

---

![](https://cdn.discordapp.com/attachments/1065385280393203892/1234201670561562725/YQgT.gif?ex=662fdf60&is=662e8de0&hm=0b3ea51e8beba7e5a01c611f5dadc01bf0b2f052049712607deca70d0d7a8897&)

```
~ It always seems impossible until it's done.

Hey there, I am a software engineer since 2021.

I am currently working on a SaaS (Nuxt.js, Golang)

I have a DEC in computer science.
```

---

```go
- My Skills
var languages = []string{
    "Golang",
    "Javascript",
    "Typescript",
    "Python",
    "C",
    "Java",
    "Sql",
    "Html/Css",
}

var knowledges = []string{
    "React",
    "Angular",
    "Next",
    "Nuxt",
    "Sass",
    "GraphQL",
    "RestAPI",
    "Express",
    "Spring",
    "ETC",
}

var tools = []string{
    "Docker",
    "PostgreSQL",
    "MongoDB",
    "Redis",
    "Vercel",
    "ETC",
}
```

---

```go
- Social
Discord: imzoloft
Telegram: imzoloft
Gitlab: imzoloft
Github: imzoloft

Revolutionize: https://discord.gg/2VDpC6xFkE
```

---

```go
Thanks for visiting my profile, you can always donate me!
BTC Wallet: bc1q0jc0dd6a7alzmr8j7hseg6r5d8333re9wu87wj
```
